require("dotenv").config();

module.exports = {
  siteMetadata: {
    title: `Reza Faiz Atta Rahman | Blog | Work | Portofolio`,
    name: `Reza Faiz Atta Rahman`,
    siteUrl: `https://reza.fyi`,
    description: `Personal website of Reza Faiz Atta Rahman used to show some works, writing and shots`,
    hero: {
      heading: `Hey, folks! 🙌 <br/><br/><br/>I'm Reza, a Designer with 4+ years of experience in interaction design, research, prototyping, and interdisciplinary teamwork. I'm passionate about design systems, digital products, and design tools. Loves to organize & turn complex problems into simpler ones, and learn a new strategy to solve them.`,
      maxWidth: 640
    },
    social: [
      {
        name: `twitter`,
        url: `https://twitter.com/rezafaizarahman`
      },
      {
        name: `github`,
        url: `https://github.com/rezafaizarahman`
      },
      {
        name: `instagram`,
        url: `https://instagram.com/rezafaizarahman`
      },
      {
        name: `linkedin`,
        url: `https://www.linkedin.com/in/rezafaizarahman`
      },
      {
        name: `dribbble`,
        url: `https://dribbble.com/rezafaizarahman`
      }
    ]
  },
  plugins: [
    {
      resolve: "@narative/gatsby-theme-novela",
      options: {
        contentPosts: "content/posts",
        contentAuthors: "content/authors",
        basePath: "/",
        authorsPage: true,
        sources: {
          local: true,
          contentful: false
        }
      }
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Reza Faiz Atta Rahman | Blog | Work | Portofolio`,
        short_name: `Reza Faiz`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#fff`,
        display: `standalone`,
        icon: `src/assets/favicon.png`
      }
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "UA-151770775-2"
      }
    }
  ]
};
